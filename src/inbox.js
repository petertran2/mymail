const MessageStore = require('./message_store.js')

module.exports = {
	render() {
		let inbox = document.createElement('ul')
		inbox.className = 'messages'
		MessageStore.getInboxMessages().forEach(message => {
			inbox.appendChild(this.renderMessage(message))
		})
		return inbox
	},
	renderMessage(message) {
		let li = document.createElement('li')
		li.className = 'message'
		li.innerHTML = `
			<span class='from'>${message.from}</span>
			<span class='subject'>${message.subject}</span>
			<span class='body'>${message.body}</span>
		`
		return li
	}
}
