const MessageStore = require('./message_store.js')

module.exports = {
	render() {
		let sentBox = document.createElement('ul')
		sentBox.className = 'messages'
		MessageStore.getSentMessages().forEach(message => {
			sentBox.appendChild(this.renderMessage(message))
		})
		return sentBox
	},
	renderMessage(message) {
		let li = document.createElement('li')
		li.className = 'message'
		li.innerHTML = `
			<span class='to'>${message.to}</span>
			<span class='subject'>${message.subject}</span>
			<span class='body'>${message.body}</span>
		`
		return li
	}
}
