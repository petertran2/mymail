let Router = require('./router.js')
let Inbox = require('./inbox.js')
let Sent = require('./sent.js')
let Compose = require('./compose.js')

let routes = {
	compose: Compose,
	inbox: Inbox,
	sent: Sent
}

document.addEventListener('DOMContentLoaded', () => {
	let router = new Router(document.querySelector('.content'), routes)
	router.start()
	window.location.hash = '#inbox'
	document.querySelectorAll('.sidebar-nav li').forEach(item => {
		item.addEventListener('click', () => {
			window.location.hash = item.innerText.toLowerCase()
		})
	})
})
