# MyMail

MyMail is a single-page webmail client written in HTML, CSS and Javascript, and deployed with Gitlab CI/CD.

[Live Demo!](https://petertran2.gitlab.io/mymail)

Front-End: HTML, CSS, Javascript\
Back-End: n/a\
Database: n/a\
Deployment: Gitlab CI/CD
