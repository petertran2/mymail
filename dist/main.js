/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/compose.js":
/*!************************!*\
  !*** ./src/compose.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const MessageStore = __webpack_require__(/*! ./message_store.js */ \"./src/message_store.js\")\n\nconst Compose = {\n\trender() {\n\t\tlet div = document.createElement('div')\n\t\tdiv.className = 'new-message'\n\t\tdiv.innerHTML = this.renderForm()\n\t\tdiv.addEventListener('change', event => {\n\t\t\tMessageStore.updateDraftField(event.target.name, event.target.value)\n\t\t})\n\t\tdiv.addEventListener('submit', event => {\n\t\t\tevent.preventDefault()\n\t\t\tMessageStore.sendDraft()\n\t\t\twindow.location.hash = '#inbox'\n\t\t})\n\t\treturn div\n\t},\n\trenderForm() {\n\t\tlet draft = MessageStore.getMessageDraft()\n\t\tlet HTML = `\n\t\t\t<p class='new-message-header'>New Message</p>\n\t\t\t<form class='compose-form'>\n\t\t\t\t<input placeholder='Recipient' name='to' type='text'\n\t\t\t\t\tvalue='${draft.to}'>\n\t\t\t\t<input placeholder='Subject' name='subject' type='text'\n\t\t\t\t\tvalue='${draft.subject}'>\n\t\t\t\t<textarea name='body' rows='20'>${draft.body}</textarea>\n\t\t\t\t<button type='submit' class='btn btn-primary submit-message'>Send\n\t\t\t\t\t</button>\n\t\t\t</form>\n\t\t`\n\t\treturn HTML\n\t}\n}\n\nmodule.exports = Compose\n\n\n//# sourceURL=webpack:///./src/compose.js?");

/***/ }),

/***/ "./src/inbox.js":
/*!**********************!*\
  !*** ./src/inbox.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const MessageStore = __webpack_require__(/*! ./message_store.js */ \"./src/message_store.js\")\n\nmodule.exports = {\n\trender() {\n\t\tlet inbox = document.createElement('ul')\n\t\tinbox.className = 'messages'\n\t\tMessageStore.getInboxMessages().forEach(message => {\n\t\t\tinbox.appendChild(this.renderMessage(message))\n\t\t})\n\t\treturn inbox\n\t},\n\trenderMessage(message) {\n\t\tlet li = document.createElement('li')\n\t\tli.className = 'message'\n\t\tli.innerHTML = `\n\t\t\t<span class='from'>${message.from}</span>\n\t\t\t<span class='subject'>${message.subject}</span>\n\t\t\t<span class='body'>${message.body}</span>\n\t\t`\n\t\treturn li\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/inbox.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("let Router = __webpack_require__(/*! ./router.js */ \"./src/router.js\")\nlet Inbox = __webpack_require__(/*! ./inbox.js */ \"./src/inbox.js\")\nlet Sent = __webpack_require__(/*! ./sent.js */ \"./src/sent.js\")\nlet Compose = __webpack_require__(/*! ./compose.js */ \"./src/compose.js\")\n\nlet routes = {\n\tcompose: Compose,\n\tinbox: Inbox,\n\tsent: Sent\n}\n\ndocument.addEventListener('DOMContentLoaded', () => {\n\tlet router = new Router(document.querySelector('.content'), routes)\n\trouter.start()\n\twindow.location.hash = '#inbox'\n\tdocument.querySelectorAll('.sidebar-nav li').forEach(item => {\n\t\titem.addEventListener('click', () => {\n\t\t\twindow.location.hash = item.innerText.toLowerCase()\n\t\t})\n\t})\n})\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/message_store.js":
/*!******************************!*\
  !*** ./src/message_store.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("class Message {\n\tconstructor(from, to = '', subject = '', body = '') {\n\t\tthis.from = from,\n\t\tthis.to = to,\n\t\tthis.subject = subject,\n\t\tthis.body = body\n\t}\n}\n\nlet messageDraft = new Message()\n\nlet messages = {\n\tsent: [\n\t\t{\tto: 'friend@mail.com', subject: 'Check this out', body: \"It's so cool\" },\n\t\t{ to: 'person@mail.com', subject: 'zzz', body: 'so booring' }\n\t\t],\n\tinbox: [\n\t\t{\n\t\t\tfrom: 'grandma@mail.com',\n\t\t\tsubject: 'Fwd: Fwd: Fwd: Check this out',\n\t\t\tbody: 'Stay at home mom discovers cure for leg cramps. Doctors hate her'\n\t\t},\n\t\t{\n\t\t\tfrom: 'person@mail.com',\n\t\t\tsubject: 'Questionnaire',\n\t\t\tbody: 'Take this free quiz win $1000 dollars'\n\t\t}\n\t]\n}\n\nconst MessageStore = {\n\tgetInboxMessages() {\n\t\treturn messages.inbox\n\t},\n\tgetSentMessages() {\n\t\treturn messages.sent\n\t},\n\tgetMessageDraft() {\n\t\treturn messageDraft\n\t},\n\tupdateDraftField(field, value) {\n\t\tmessageDraft[field] = value\n\t},\n\tsendDraft() {\n\t\tmessages.sent.push(messageDraft)\n\t\tmessageDraft = new Message()\n\t}\n}\n\nmodule.exports = MessageStore\n\n\n//# sourceURL=webpack:///./src/message_store.js?");

/***/ }),

/***/ "./src/router.js":
/*!***********************!*\
  !*** ./src/router.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("class Router {\n\tconstructor(node, routes) {\n\t\tthis.node = node\n\t\tthis.routes = routes\n\t}\n\t\n\tstart() {\n\t\tthis.render()\n\t\twindow.addEventListener('hashchange', () => this.render())\n\t}\n\t\n\tactiveRoute() {\n\t\treturn this.routes[window.location.hash.replace('#', '')]\n\t}\n\t\n\trender() {\n\t\tthis.node.innerHTML = ''\n\t\tlet component = this.activeRoute()\n\t\tif (component) {\n\t\t\tthis.node.appendChild(component.render())\n\t\t}\n\t}\n}\n\nmodule.exports = Router\n\n\n//# sourceURL=webpack:///./src/router.js?");

/***/ }),

/***/ "./src/sent.js":
/*!*********************!*\
  !*** ./src/sent.js ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("const MessageStore = __webpack_require__(/*! ./message_store.js */ \"./src/message_store.js\")\n\nmodule.exports = {\n\trender() {\n\t\tlet sentBox = document.createElement('ul')\n\t\tsentBox.className = 'messages'\n\t\tMessageStore.getSentMessages().forEach(message => {\n\t\t\tsentBox.appendChild(this.renderMessage(message))\n\t\t})\n\t\treturn sentBox\n\t},\n\trenderMessage(message) {\n\t\tlet li = document.createElement('li')\n\t\tli.className = 'message'\n\t\tli.innerHTML = `\n\t\t\t<span class='to'>${message.to}</span>\n\t\t\t<span class='subject'>${message.subject}</span>\n\t\t\t<span class='body'>${message.body}</span>\n\t\t`\n\t\treturn li\n\t}\n}\n\n\n//# sourceURL=webpack:///./src/sent.js?");

/***/ })

/******/ });